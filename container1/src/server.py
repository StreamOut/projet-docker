from flask import Flask, json
from flask import request
from flask_mysqldb import MySQL
from flask_cors import CORS

############################################################################### SERVER ###########################################################################
#                                                                                                                                                                #
# - Flask expose le serveur sur le port 5000                                                                                                                     #
#                                                                                                                                                                #
#                                                                                                                                                                #
##################################################################################################################################################################

api = Flask(__name__)
CORS(api)

users = [{"id": 1, "name": "Billy"}, {"id": 2, "name": "Jim"}]
idCount = 2

#### ETAPE 3 #####
# api.config['MYSQL_HOST'] = 'db'
# api.config['MYSQL_USER'] = 'root'
# api.config['MYSQL_PASSWORD'] = 'somepassword'
# api.config['MYSQL_DB'] = 'MyDB'
#
#mysql = MySQL(api)

@api.route('/users', methods=['GET'])
def get_users():
    result = users
    #### ETAPE 3 ####
    # cur = mysql.connection.cursor()
    # cur.execute("SELECT * FROM USER")
    # mysql.connection.commit()
    # result = cur.fetchall()
    # cur.close()
    return json.dumps(result)

@api.route('/users', methods=['POST'])
def post_users():
    global idCount
    idCount = idCount + 1
    data = request.form
    users.append({"id": idCount, "name": data["name"]})
    #### ETAPE 3 ####
    # cur = mysql.connection.cursor()
    # cur.execute("INSERT INTO USER(name) VALUES (%s)", (data["name"],))
    # mysql.connection.commit()
    # cur.close()
    return json.dumps({"success": True}), 201

if __name__ == '__main__':
    api.run(host='0.0.0.0')
